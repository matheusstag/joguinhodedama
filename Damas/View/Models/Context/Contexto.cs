﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using View.Models.Entities;

namespace View.Models.Context
{
    public class Contexto : DbContext
    {
        public Contexto()
            : base("Dama")
        {

        }

        public DbSet<Jogador> Jogadores { get; set; }
        public DbSet<Jogo> Jogos { get; set; }        
    }
}