﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace View.Models.Entities
{
    public class Jogador
    {
        [Key]
        public int Id { get; set; }

        [Required]
        public string Nome { get; set; }

        [Required]
        public DateTime Cadastro { get; set; }

        [Required]
        public int Vitorias { get; set; }

        [Required]
        public int Derrotas { get; set; }

        [Required]
        public int Empates { get; set; }

        public Jogador()
        {
            Cadastro = DateTime.Now;
            Vitorias = 0;
            Derrotas = 0;
            Empates = 0;
        }
    }
}