﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace View.Models.Entities
{
    public class Jogo
    {
        [Key]
        public int Id { get; set; }

        public enum Tipos {  Damas, Xadrez }

        public Tipos Tipo { get; set; }

        [Required()]
        public DateTime Inicio { get; set; }
        
        public DateTime? Termino { get; set; }
        
        public TimeSpan? Duracao { get; set; }
        
        public Jogador Vencedor { get; set; }
        
        public Jogador Perdedor { get; set; }

        [Required()]
        public Jogador JogadorUm { get; set; }

        [Required()]
        public Jogador JogadorDois { get; set; }

        public Jogo()
        {
            Inicio = DateTime.Now;
            Termino = null;
            Duracao = Termino - Inicio;
        }
    }
}